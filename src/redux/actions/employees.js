import axios from 'axios';
import { employeesUrl, employeeUrl, locationsUrl, positionsUrl } from '../../urls';

export const getEmployees = () => dispatch => {
  return axios.get(employeesUrl())
    .then(({ data }) => {
      dispatch({
        type: 'GET_EMPLOYEES',
        payload: data
      });
    });
};

export const addEmployee = employee => dispatch => {
  return axios.post(employeesUrl(), employee)
    .then(() => 
      dispatch(getEmployees())
    );
};

export const updateEmployee = employee => dispatch => {
  return axios.put(employeeUrl(employee.id), { ...employee })
    .then(res => {
      const user = res.data.filter(
        index => {
          return index.id === employee.id;
        });

      dispatch({
        type: 'UPDATED_EMPLOYEE',
        payload: user[0]
      });
    });
};

export const removeEmployee = id => dispatch => {
  return axios.delete(employeeUrl(id))
    .then(() => 
      dispatch(getEmployees())
    );
};

export const getLocations = () => dispatch => {
  axios.get(locationsUrl())
    .then(({ data }) => 
      dispatch({
        type: 'GET_LOCATIONS',
        payload: data
      })
    );
};

export const getPositions = () => dispatch => {
  return axios.get(positionsUrl())
    .then(({ data }) => 
      dispatch({
        type: 'GET_POSITIONS',
        payload: data
      })
    );
};