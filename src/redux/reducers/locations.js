const initialState =  {
  locations: []
};

const locations = (state = initialState, action) => {
  switch (action.type) {
    case 'GET_LOCATIONS':
      return { ...state, locations: action.payload };
    default:
      return state;
  }
};

export default locations;