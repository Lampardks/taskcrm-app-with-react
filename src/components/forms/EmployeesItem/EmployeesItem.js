import React, { Component, Fragment } from 'react';
import { Card, Image, Icon, Grid, Modal, Loader } from 'semantic-ui-react';
import PropTypes from 'prop-types';

import EmployeeEdit from '../../modal/EmployeeEdit/EmployeeEdit';
import EmployeeDelete from '../../modal/EmployeeDelete/EmployeeDelete';

import formatDate from '../../../utils/formatDate';

import './EmployeesItem.scss';

class EmployeesItem extends Component {

  state = {
    modalOpen: false,
    deleteOpen: false,
    isLoading: false
  }

  handleOpen = () => this.setState({ modalOpen: true });
  handleOpenDel = () => this.setState({ deleteOpen: true });

  handleClose = () => this.setState({ modalOpen: false });
  handleCloseDel = () => this.setState({ deleteOpen: false });

  idUpdate = id => {
    /* eslint-disable no-console */
    // console.log(id === this.props.id);
    return id === this.props.id 
      ? this.setState({ isLoading: true })
      : this.setState({ isLoading: false });
  };

  onRemove = () => {
    this.handleCloseDel();
    /* eslint-disable no-console */
    // console.log(this.props.id);

    this.props.onRemove(this.props.id);
  }

  render () {
    const {
      id,
      name,
      surName,
      avatar,
      email,
      birthday,
      position,
      location,
      locationId,
      positionId,
      updateEmployee,
      loading
    } = this.props;
    const { isLoading } = this.state;    

    return (
      <Fragment>
        <Grid.Column width={4}>
          <Card>
            {loading && isLoading && <Loader active />}
            <div className='EmployeesItem-block-img'>
              <Image 
                className='EmployeesItem-block-img__image'
                size='medium' 
                src={avatar ? avatar : 'https://tracker.moodle.org/secure/attachment/30912/f3.png' } />
            </div>
            <Card.Content className='EmployeesItem-block-content'>
              <Card.Header>{name} {surName}</Card.Header>
              {!!birthday &&
              <Card.Meta>
                <span className='date'>Birthday: {formatDate(birthday)}</span>
              </Card.Meta>
              }      
              <Card.Description>{position ? position.name : 'Not position'}</Card.Description>
              <Card.Description>{location ? location.name : 'Not location'}</Card.Description>    
            </Card.Content>
            <Card.Content extra>
              <Modal 
                centered={false}
                open={this.state.modalOpen}
                onClose={this.handleClose}
                trigger={<a onClick={this.handleOpen} title='Edit an employee card'>
                  <Icon name='edit' />
                </a>}>

                <EmployeeEdit 
                  id={id}
                  name={name}
                  surName={surName}
                  email={email}
                  avatar={avatar}
                  birthday={birthday}
                  position={position}
                  positionId={positionId}
                  location={location}
                  locationId={locationId}
                  updateEmployee={updateEmployee}
                  handleClose={this.handleClose}
                  idUpdate={this.idUpdate} />
              </Modal>
              <Modal 
                centered={false}
                open={this.state.deleteOpen}
                onClose={this.handleCloseDel}
                trigger={<a onClick={this.handleOpenDel} title='Delete employee'>
                  <Icon name='remove user' />
                </a>}>
                <EmployeeDelete 
                  id={id} 
                  name={name}
                  surName={surName}
                  handleClose={this.handleCloseDel}
                  onRemove={this.onRemove} />
              </Modal>
            </Card.Content>
          </Card>
        </Grid.Column>
      </Fragment>
    );
  }  
}

EmployeesItem.propTypes = {
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  surName: PropTypes.string.isRequired,
  avatar: PropTypes.string,
  birthday: PropTypes.string,
  position: PropTypes.object,
  location: PropTypes.object,
  locationId: PropTypes.string,
  updateEmployee: PropTypes.func.isRequired,
  positionId: PropTypes.string,
  email: PropTypes.string.isRequired,
  loading: PropTypes.bool.isRequired,
  onRemove: PropTypes.func.isRequired
};

export default EmployeesItem;