import axios from 'axios';
import { employeeUrl } from '../../urls';
import { UPDATE_PHOTO } from '../actionTypes';

export const updatedPhoto = avatar => ({
  type: UPDATE_PHOTO,
  payload: avatar
});

export const updatePhotoAct = data => dispatch => {
  return axios.put(employeeUrl(data.id), { id: data.id, avatar: data.result })
    .then(res => {
      const user = res.data.filter(
        index => {
          return index.id === data.id;
        });

      dispatch(updatedPhoto(user[0]));
    });
};