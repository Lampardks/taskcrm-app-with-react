import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Segment, Image, Grid, Card, Loader, Container, Header, List } from 'semantic-ui-react';

import formatDate from '../../../utils/formatDate';
import UpdatePhoto from '../UpdatePhoto/UpdatePhoto';
import ProjectList from '../ProjectList/ProjectList';

import { updatePhotoAct } from '../../../redux/actions/profile';
import { getProjects } from '../../../redux/actions/projects';
import { getEmployeeTasks } from '../../../redux/actions/tasks';

import './CardUser.scss';

class CardUser extends Component {

  constructor (props) {
    super(props);

    this.state = { 
      loading: false,
      loadingAll: false
    }; 
  }
  // static defaultProps = {
  //   user: {
  //     avatar: 'https://tracker.moodle.org/secure/attachment/30912/f3.png'
  //   }    
  // }

  componentDidMount () {
    this.setState({ loadingAll: true });

    this.props.dispatch(getProjects())
      .then(() => 
        this.props.dispatch(getEmployeeTasks(this.props.user.id))
          .then(() => this.setState({ loadingAll: false }))        
      );
  }

  updatePhoto = event => {
    this.setState({ loading: true });
    const avatar = event.target.files[0];
    const reader = new FileReader();

    reader.readAsDataURL(avatar);
    reader.onload = () => {
      const { id } = this.props.user;
      const { result } = reader;

      this.props
        .dispatch(updatePhotoAct({ id, result }))
        .then(() => {
          this.setState({ loading: false });
        });
    };
  }

  render () {
    let { avatar } = this.props.user;
    const { name, surName, birthday, location, position, id } = this.props.user;
    const { projects, tasks } = this.props;

    const { loading } = this.state;

    const projectsEmployee = projects.filter((project) => {
      return project.employees.includes(id);
    });

    let projectsEmployee1 = [];

    projectsEmployee.forEach((project, i) => {
      const taskProjectEmployee = tasks.filter((task) => {
        return project.id === task.projectId;
      });

      if (taskProjectEmployee !== undefined) {
        projectsEmployee1.push(
          <ProjectList 
            key={i}
            project={project}
            taskProjectEmployee={taskProjectEmployee} />
        );
      }
    });



    /* eslint-disable no-console */
    // console.log(projectsEmployee1);
    /* eslint-enable no-console */

    if (avatar.length === 0) {
      avatar = 'https://tracker.moodle.org/secure/attachment/30912/f3.png';
    }

    return (
      <Segment>
        <Grid>
          <Grid.Column className='card-user__img-width'>
            <div>
              {loading && <Loader active />}
              <Image src={avatar} size='small' />
            </div>
            <UpdatePhoto updatePhoto={this.updatePhoto} />            
          </Grid.Column>
          <Grid.Column width={13}>
            <Grid>
              <Grid.Column width={3}>
                <Card.Header>{name} {surName}</Card.Header>
                {!!birthday &&
                  <Card.Meta>
                    <span className='date'>Birthday: {formatDate(birthday)}</span>
                  </Card.Meta>
                }
                {!!location && 
                  <Card.Description>
                    Location: {location.name}
                  </Card.Description>
                }
              </Grid.Column>
              {!!position &&
              <Grid.Column width={1}>
                {position.name}
              </Grid.Column> 
              }
            </Grid>           
          </Grid.Column>
        </Grid>
        <Container text className='Projects-line'>
          <Header as='h1'>Projects</Header>
          <List divided relaxed>
            {projectsEmployee1}
          </List>
        </Container>
      </Segment>
    );
  }
}

CardUser.propTypes = {
  user: PropTypes.shape({
    name: PropTypes.string,
    avatar: PropTypes.data,
    surName: PropTypes.string,
    birthday: PropTypes.date,
    location: PropTypes.object,
    position: PropTypes.object,
    id: PropTypes.string.isRequired
  }),
  dispatch: PropTypes.func.isRequired,
  projects: PropTypes.array.isRequired,
  tasks: PropTypes.array.isRequired
};

const mapStateToProps = (state) => ({
  user: state.user.user,
  projects: state.projects.projects,
  tasks: state.tasks.tasks
});

export default connect(mapStateToProps)(CardUser);