export default date => {
  const dateArray = date.split('.');
  const dateString = dateArray[1] + '/' + dateArray[0] + '/' + dateArray[2];
  const newDate = new Date(dateString);

  return newDate;
};