import axios from 'axios';
import { employeesUrl, authUrl } from '../urls';
// import { GET_EMPLOYEES, LOG_IN } from './actionTypes';

export const getEmployees = () => dispatch => {
  return axios.get(employeesUrl())
    .then(({ data }) => {
      // dispatch(data);
      return dispatch(setContacts(data));
    });
};

export const setContacts = employees => ({
  type: 'GET_EMPLOYEES',
  payload: employees
});

// export const logIn = (email, password) => ({
//   type: 'LOG_IN',
//   payload: { email, password }
// });

export const logIn = (email, password) => dispatch => {
  axios.post(authUrl(), { email, password })
    .then(({ data }) => {
      dispatch({
        type: 'LOG_IN',
        payload: data
      });
    })
    .catch(({ response }) => {
      dispatch({
        type: 'LOG_IN_ERROR',
        payload: response.data
      });
    });
};

export const logOut = () => ({
  type: 'LOG_OUT'
});