import React, { Component } from 'react';
import { Grid } from 'semantic-ui-react';
import HTML5Backend from 'react-dnd-html5-backend';
import { DragDropContext } from 'react-dnd';
import PropTypes from 'prop-types';

import StatusTasksContainer from './StatusTasksContainer';

import './TasksContainer.scss';

class TasksContainer extends Component {
  render () {
    const { 
      tasks, 
      statuses, 
      employees, 
      moveCard, 
      typeTask, 
      onRemove,
      updateTask 
    } = this.props;

    return (
      <Grid className='Task-page__dnd-container'>
        {statuses.map((status, i) =>
          <StatusTasksContainer 
            key={status.id}
            id={status.id}
            moveCard={moveCard}
            status={status}
            tasks={tasks}
            employees={employees}
            typeTask={typeTask}
            x={i}
            onRemove={onRemove}
            updateTask={updateTask} />            
        )} 
      </Grid>
    );
  }
}

TasksContainer.propTypes = {
  tasks: PropTypes.array.isRequired,
  statuses: PropTypes.array.isRequired,
  employees: PropTypes.array.isRequired,
  typeTask: PropTypes.array,
  moveCard: PropTypes.func.isRequired,
  onRemove: PropTypes.func.isRequired,
  updateTask: PropTypes.func.isRequired
};

export default DragDropContext(HTML5Backend)(TasksContainer);