import React from 'react';
import PropTypes from 'prop-types';
// import { Modal } from 'semantic-ui-react';

import './UpdatePhoto.scss';

const UpdatePhoto = ({ updatePhoto }) => (
  <div className='update-photo'>
    <span className='update-photo__title'>Update profile photo</span>
    <div className='update-photo__input'>
      <a className='update-photo__link'>
        <div className='update-photo__link-block'>
          <input type="file" onChange={updatePhoto} />
        </div>
      </a>
    </div>                  
  </div>
);

UpdatePhoto.propTypes = {
  updatePhoto: PropTypes.func.isRequired
};

export default UpdatePhoto;