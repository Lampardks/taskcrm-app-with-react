import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Form, Button, Segment, Message } from 'semantic-ui-react';
import Validator from 'validator';

import InlineError from '../messages/InlineError';

import './LoginForm.scss';

class LoginForm extends Component {
  state = {
    data: {
      email: '',
      password: ''
    },
    loading: false,
    errors: {}
  }

  onChange = event =>
    this.setState({
      data: { ...this.state.data, [event.target.name]: event.target.value }
    });

  onSubmit = (event) => {
    event.preventDefault();
    const errors = this.validate(this.state.data);

    this.setState({ errors });
    if (Object.keys(errors).length === 0) {
      this.setState({ loading: true });
      this.props
        .submit(this.state.data)
        .catch(err => 
          this.setState({ errors: err.response.data, loading: false })
        );
    }
  }

  validate = data => {
    const errors = {};

    if (!Validator.isEmail(data.email)) errors.email = 'Enter your email';
    if (!data.password) errors.password = 'Enter password';
    return errors;
  }

  render () {
    const { data, errors, loading } = this.state;

    /* eslint-disable no-console */
    // console.log(this.props.);
    /* eslint-enable no-console */

    return (
      <Form size="large" onSubmit={this.onSubmit} loading={loading}>
        <Segment stacked>
          {errors.error && (
            <Message negative>
              <Message.Header>Something went wrong</Message.Header>
              <p>{errors.error}</p>
            </Message>
          )}
          <Form.Field error={!!errors.email}>
            <label htmlFor="email">Email</label>
            <Form.Input 
              fluid 
              icon="user" 
              iconPosition="left"
              id="email" 
              name="email"
              placeholder="Email"
              value={data.email}
              onChange={this.onChange} />
            {errors.email && <InlineError text={errors.email} />}
          </Form.Field>
          <Form.Field error={!!errors.password}>
            <label htmlFor="password">Password</label>
            <Form.Input
              fluid
              icon="lock"
              iconPosition="left" 
              id="password" 
              name="password"
              type="password" 
              placeholder="Password" 
              value={data.password}
              onChange={this.onChange} />
            {errors.password && <InlineError text={errors.password} />}
          </Form.Field>
          <Button color="teal" fluid size="large">Log in</Button>
        </Segment>
      </Form>
    );
  }
}

LoginForm.propTypes = {
  submit: PropTypes.func.isRequired,
  form: PropTypes.object
};

export default LoginForm;