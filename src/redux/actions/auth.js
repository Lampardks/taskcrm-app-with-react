import axios from 'axios';
import { authUrl } from '../../urls';
// import api from '../../api';
import { USER_LOGGED_IN, USER_LOGGED_OUT } from '../actionTypes';
import setAuthorizationHeader from '../../utils/setAuthorizationHeader';

export const userLoggedIn = user => ({
  type: USER_LOGGED_IN,
  user
});

export const userLoggedOut = () => ({
  type: USER_LOGGED_OUT
});

export const login = credentials => dispatch =>
  axios.post(authUrl(), credentials)
    .then(res => {
      const { user, token } = res.data;

      localStorage.taskCRMToken = token;
      localStorage.taskCRMUser = JSON.stringify(user);
      setAuthorizationHeader(token);
      dispatch(userLoggedIn({ user, isAuthenticated: true }));
    });

// export const login = credentials => dispatch =>
//   api.user.login(credentials).then(user => {

//     /* eslint-disable no-console */
//     console.log(user);
//     const { token } = user;

//     localStorage.taskCRMToken = token;
//     localStorage.taskCRMUser = JSON.stringify(user.user);
//     setAuthorizationHeader(token);
//     dispatch(userLoggedIn({ ...user, isAuthenticated: true }));
//   });

export const logout = () => dispatch => {
  localStorage.removeItem('taskCRMToken');
  localStorage.removeItem('taskCRMUser');
  setAuthorizationHeader();
  dispatch(userLoggedOut());
};

/* eslint-disable no-console */
// console.log(data);
/* eslint-enable no-console */
