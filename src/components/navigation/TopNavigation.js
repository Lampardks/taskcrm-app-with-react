import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Menu, Icon } from 'semantic-ui-react';
import { Link } from 'react-router-dom';

import * as actions from '../../redux/actions/auth';

const TopNavigation = ({ logout, user }) => (  
  <Menu inverted color='teal'>
    <Menu.Item as={Link} to='/projects'>
      <Icon name='cogs' />
      Projects
    </Menu.Item>
    <Menu.Item as={Link} to='/employees'>
      <Icon name='users' />
      Employees
    </Menu.Item>
    <Menu.Menu position='right'>
      <Menu.Item as={Link} to='/'>
        <Icon name='id card' />
        {user.name} {user.surName}
      </Menu.Item>
      <Menu.Item onClick={() => logout()}>
        <Icon name='log out' />
        Logout
      </Menu.Item>
    </Menu.Menu>
  </Menu>
);

/* eslint-disable no-console */
// console.log(user.name);
/* eslint-enable no-console */

TopNavigation.propTypes = {
  logout: PropTypes.func.isRequired,
  user: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
  user: state.user.user
});

export default connect(mapStateToProps, { logout: actions.logout })(TopNavigation);
