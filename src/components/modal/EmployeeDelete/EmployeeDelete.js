import React, { Fragment } from 'react';
import { Modal, Button } from 'semantic-ui-react';
import PropTypes from 'prop-types';

// import './EmployeeAdd.scss';

const EmployeeDelete = ({
  // id,
  name,
  surName,
  handleClose,
  onRemove
}) => {

  // onDelete = (event) => {
  //   event.preventDefault();
  

  // };

  // handleClose = () => this.props.handleClose();

  return (
    <Fragment>
      <Modal.Header>Delete employee {name} {surName}</Modal.Header>
      <Modal.Content>
        <p>Are you sure you want to delete employee {name} {surName}</p>
      </Modal.Content>
      <Modal.Actions>
        <Button onClick={handleClose} negative>
          No
        </Button>
        <Button
          positive
          labelPosition='right'
          icon='checkmark'
          content='Yes'
          onClick={onRemove} />
      </Modal.Actions>
    </Fragment>
  );
};

EmployeeDelete.propTypes = {
  // id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  surName: PropTypes.string.isRequired,
  handleClose: PropTypes.func.isRequired,
  onRemove: PropTypes.func.isRequired
};

export default EmployeeDelete;