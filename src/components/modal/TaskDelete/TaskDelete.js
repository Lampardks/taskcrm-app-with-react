import React from 'react';
import { Modal, Button, Icon } from 'semantic-ui-react';
import PropTypes from 'prop-types';

const EmployeeDelete = ({
  name,
  handleClose,
  onRemove,
  deleteOpen,
  handleOpenDel
}) => {

  return (
    <Modal 
      centered={false}
      open={deleteOpen}
      onClose={handleClose}
      trigger={
        <a onClick={handleOpenDel} className='Delete-task__icon' title='Delete task'>
          <Icon name='delete' />
        </a>
      }>
      <Modal.Header>Delete task</Modal.Header>
      <Modal.Content>
        <p>Are you sure you want to delete task: {name}</p>
      </Modal.Content>
      <Modal.Actions>
        <Button onClick={handleClose} negative>
          No
        </Button>
        <Button
          positive
          labelPosition='right'
          icon='checkmark'
          content='Yes'
          onClick={onRemove} />
      </Modal.Actions>
    </Modal>
  );
};

EmployeeDelete.propTypes = {
  name: PropTypes.string.isRequired,
  handleClose: PropTypes.func.isRequired,
  onRemove: PropTypes.func.isRequired,
  handleOpenDel: PropTypes.func.isRequired,
  deleteOpen: PropTypes.bool.isRequired
};

export default EmployeeDelete;