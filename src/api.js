import axios from 'axios';
import { authUrl } from './urls';

export default {
  user: {
    login: credentials => {
      /* eslint-disable no-console */
      console.log(credentials);
      /* eslint-disable no-console */
      console.log(axios.defaults.headers);
      return axios.post(authUrl(), credentials).then(res => res.data);
    }      
  }
};