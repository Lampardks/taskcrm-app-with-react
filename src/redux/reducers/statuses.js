const initialState =  {
  statuses: []
};

const statuses = (state = initialState, action) => {
  switch (action.type) {
    case 'GET_STATUSES':
      return { ...state, statuses: action.payload };
    default:
      return state;
  }
};

export default statuses;