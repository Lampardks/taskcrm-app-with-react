import React, { Component } from 'react';
import { Modal, Form, Button, Icon, TextArea } from 'semantic-ui-react';
import { Select } from 'antd';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import InlineError from '../../messages/InlineError';

import { getEmployees } from '../../../redux/actions/employees';

// import convertDate from '../../../utils/convertDate';

// import './TaskAdd.scss';

class ProjectAdd extends Component {
  state = {
    project: {
      employees: [],
      creationDate: new Date(),
      name: '',
      description: ''
    },
    modalOpen: false,
    loading: false,
    errors: {}
  }

  componentDidMount () {
    this.props.dispatch(getEmployees());
  }

  onChange = event =>
    this.setState({
      project: { ...this.state.project, [event.target.name]: event.target.value }
    });

  handleChangeEmployees = (value) => {
    this.setState({ project: { ...this.state.project, employees: value } });

    /* eslint-disable no-console */
    console.log(value);
  }

  onSubmit = (event) => {
    event.preventDefault();

    const errors = this.validate(this.state);

    this.setState({ errors });
    if (Object.keys(errors).length === 0) {
      this.handleClose();

      // const { creationDate } = this.state.project;

      console.log(this.state.project);
      this.props.addProject({ 
        ...this.state.project
      });
    }    
  };

  handleOpen = () => this.setState({ modalOpen: true });

  handleClose = () => this.setState({ modalOpen: false });

  validate = data => {
    const { name, description } = data.project;
    const errors = {};

    if (!name) errors.name = 'Enter project name';
    if (!description) errors.surName = 'Enter description';
    return errors;
  }

  render () {
    const { errors, modalOpen } = this.state;
    const { employees } = this.props;
  
    return (
      <Modal 
        centered={false}
        open={modalOpen}
        onClose={this.handleClose}
        trigger={
          <Button 
            onClick={this.handleOpen} 
            className='Add-task__button' 
            color='green' 
            size='small'>
            <Icon name='add circle'/>Add new project
          </Button>
        }>
        <Modal.Header>Add new project</Modal.Header>
        <Modal.Content image>
          <Form className='' onSubmit={this.onSubmit}>
            <div className='Add-task-container'>
              <Modal.Description className='Add-task-container__item'>
                <Form.Field error={!!errors.name}>
                  <label htmlFor="name"></label>
                  <Form.Input 
                    fluid 
                    name="name"
                    placeholder='Name task'
                    onChange={this.onChange} />
                  {errors.name && <InlineError text={errors.name} />}
                </Form.Field>               

                <Form.Field>
                  <label htmlFor="employees"></label>
                  <Select 
                    mode="multiple"
                    size='default'
                    id='employees'
                    placeholder='Employees'
                    style={{ width: 203.39, height: 38 }} 
                    onChange={this.handleChangeEmployees}>
                    {employees.map((index) => 
                      <Select.Option 
                        key={index.id}>
                        {index.name + ' ' + index.surName}
                      </Select.Option>            
                    )}                
                  </Select>
                </Form.Field>              
              </Modal.Description>             
            
              <Modal.Description className='Add-task-container__item'>
                <Form.Field error={!!errors.description}>
                  <label htmlFor="description"></label>
                  <TextArea 
                    name="description"
                    id='description'
                    placeholder='Description'
                    style={{ minHeight: 142, minWidth: 245 }}
                    onChange={this.onChange} />
                  {errors.description && <InlineError text={errors.description} />}
                </Form.Field>
              </Modal.Description>
            </div>
            <div className='Add-task__button-popup'>
              <Button className='Add-task__button-popup__button' color='teal' size='large'>
                <Icon name='save'/>Save
              </Button>
            </div>        
          </Form>
        </Modal.Content>
      </Modal>
    );
  }  
}

ProjectAdd.propTypes = {
  dispatch: PropTypes.func.isRequired,
  employees: PropTypes.array.isRequired,
  addProject: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  employees: state.employees.employees
});

export default connect(mapStateToProps)(ProjectAdd);