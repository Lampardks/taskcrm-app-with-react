export default (a, b) => {
  return a.sortId - b.sortId;
};