// import { compareNumeric } from '../../utils/compareNumeric';

const initialState =  {
  tasks: []
};

const tasks = (state = initialState, action) => {
  switch (action.type) {
    case 'GET_TASKS': {
      const { payload } = action;
      
      const compareNumeric = (a, b) => {
        return a.sortId - b.sortId;
      };

      payload.sort(compareNumeric);

      return { ...state, tasks: payload };
    }
    case 'MOVE_TASK': {
      const { lastX, lastY, nextX, nextY } = action.payload;
      const { tasks } = state;

      /* eslint-disable no-console */
      // console.log(lastX, lastY, nextX, nextY);

      const filterStatus = tasks.filter((task) => {
        return task.statusId === lastX && task.sortId === lastY;
      });

      const filteredStatusOther = tasks.filter((task) => {
        return task.id !== filterStatus[0].id;
      });

      const newSortTask = {
        ...filterStatus[0],
        statusId: nextX,
        sortId: nextY
      };

      const newTasks = [
        ...filteredStatusOther,
        newSortTask
      ];
      
      return { ...state, tasks: newTasks };
    }
    case 'UPDATED_TASK': {
      const { tasks } = state;
      const { payload } = action;

      /* eslint-disable no-console */
      // console.log(payload);

      for (let i = 0; i < tasks.length; i++) {
        if (tasks[i].id === payload.id) {
          tasks[i] = payload;
          break;
        }
      }

      console.log(tasks);
      return { ...state, tasks };
    }
    default:
      return state;
  }
};

export default tasks;