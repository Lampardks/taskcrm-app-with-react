import React, { Component } from 'react';
import PropTypes from 'prop-types';

import TaskEdit from '../modal/TaskEdit/TaskEdit';
import TaskDelete from '../modal/TaskDelete/TaskDelete';

class Task extends Component {
  render () {
    const { 
      style, 
      task, 
      employee, 
      typeTask,
      handleCloseDel, 
      onRemove, 
      handleOpenDel, 
      deleteOpen,
      updateTask,
      typeTasks,
      employees
    } = this.props;

    return (
      <div style={style} className="task-container" id={style ? task.id : null}>
        <div className="task-container__type">{ typeTask !== undefined ? typeTask.name : null }</div>
        <div>
          <div className="task-container__title">{task.name}</div>
          <div className="task-container__description">{task.description}</div>
          <div className="task-container__employee">
            <img className='task-container__employee__img' src={employee.avatar} />
            <span className='task-container__employee__name'>{employee.name} {employee.surName}</span>
            <TaskEdit 
              task={task}
              typeTasks={typeTasks}
              typeTaskName={typeTask.name}
              employee={employee}
              employees={employees}
              updateTask={updateTask} />
            <TaskDelete 
              name={task.name}
              deleteOpen={deleteOpen}
              handleOpenDel={handleOpenDel}
              handleClose={handleCloseDel}
              onRemove={onRemove} />          
          </div>
        </div>
      </div>
    );
  }
}

Task.propTypes = {
  task: PropTypes.object.isRequired,
  employee: PropTypes.object,
  typeTask: PropTypes.object,
  style: PropTypes.object,
  handleOpenDel: PropTypes.func.isRequired,
  handleCloseDel: PropTypes.func.isRequired,
  onRemove: PropTypes.func.isRequired,
  deleteOpen: PropTypes.bool.isRequired,
  updateTask: PropTypes.func.isRequired,
  typeTasks: PropTypes.array.isRequired,
  employees: PropTypes.array.isRequired
};

export default Task;
