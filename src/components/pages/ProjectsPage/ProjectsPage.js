import React, { Component } from 'react';
import { Segment, List, Loader } from 'semantic-ui-react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import ProjectsItem from '../../forms/ProjectsItem/ProjectsItem';
import ProjectAdd from '../../modal/ProjectAdd/ProjectAdd';

import { getProjects, addProject } from '../../../redux/actions/projects';

import './ProjectPage.scss';

class ProjectsPage extends Component {
  state = {
    loading: false
  }

  componentDidMount () {
    this.setState({ loading: true });

    this.props.dispatch(getProjects())
      .then(() => 
        this.setState({ loading: false })
      );
  }

  addProject = project => {
    /* eslint-disable no-console */
    console.log(project);

    this.props.dispatch(addProject(project));
  }

  render () {
    const { projects } = this.props;
    const { loading } = this.state;

    /* eslint-disable no-console */
    console.log(projects);

    return (
      <Segment padded='very' className="Segment-padding">
        <div className='Task-page__header'>
          <ProjectAdd 
            addProject={this.addProject} />
        </div>
        {loading && <Loader active />}
        <List divided relaxed>
          {projects.map(project => 
            <ProjectsItem             
              key={project.id}
              {...project} />
          )} 
        </List>
      </Segment>
    );
  }
}

ProjectsPage.propTypes = {
  dispatch: PropTypes.func.isRequired,
  projects: PropTypes.array.isRequired
};

const mapStateToProps = (state) => ({
  projects: state.projects.projects
});

export default connect(mapStateToProps)(ProjectsPage);