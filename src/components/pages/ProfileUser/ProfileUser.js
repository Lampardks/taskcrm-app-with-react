import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';

import CardUser from '../../forms/CardUser/CardUser';

import './ProfileUser.scss';

const ProfileUser = ({ isAuthenticated }) => (
  <Fragment>
    {isAuthenticated 
      ? <CardUser />
      : <Redirect to='/auth' />
    }
  </Fragment>
);

ProfileUser.propTypes = {
  isAuthenticated: PropTypes.bool.isRequired
};

const mapStateToProps = (state) => ({
  isAuthenticated: !!state.user.isAuthenticated
});

export default connect(mapStateToProps)(ProfileUser);
