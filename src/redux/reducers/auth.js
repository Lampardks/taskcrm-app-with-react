// import { USER_LOGGED_IN } from '../actionTypes';

const user = (state = {}, action = {}) => {
  switch (action.type) {
    case 'USER_LOGGED_IN': 
    {
      const { user } = action;

      return user;
    }
    // return action.user;
    case 'USER_LOGGED_OUT':
      return { isAuthenticated: false };
    case 'UPDATE_PHOTO':
      return { ...state, user: { ...action.payload } };
    default:
      return state;
  }
};

export default user;