import React, { Component } from 'react';
import { List, Loader } from 'semantic-ui-react';
import PropTypes from 'prop-types';

import TaskItem from '../TaskItem/TaskItem';

// import './ProjectsItem.scss';

class ProjectsList extends Component {  

  state = {
    isLoading: false
  }

  render () {
    const { project, taskProjectEmployee } = this.props;
    const { isLoading } = this.state;

    /* eslint-disable no-console */
    console.log(taskProjectEmployee);

    return (
      <List.Item className='Projects-list__item'>
        {isLoading && <Loader active />}
        <List.Icon name='file code' size='large' verticalAlign='middle' />
        <List.Content>
          <List.Header>{project.name}</List.Header>
          {taskProjectEmployee.map((task) => {
            <TaskItem 
              key={task.id}
              taskName={task.name} />;
          })}
        </List.Content>
      </List.Item>
    );
  }  
}

ProjectsList.propTypes = {
  taskProjectEmployee: PropTypes.array.isRequired,
  project: PropTypes.object.isRequired
};

export default ProjectsList;