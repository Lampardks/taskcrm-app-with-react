import React, { Component, Fragment } from 'react';
import { Image, Modal, Form, Button, Icon, Loader } from 'semantic-ui-react';
import { Select } from 'antd';
import { DateInput } from 'semantic-ui-calendar-react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Validator from 'validator';

import { getLocations, getPositions } from '../../../redux/actions/employees';

import formatDate from '../../../utils/formatDate';
import convertDate from '../../../utils/convertDate';
import UpdatePhoto from '../../forms/UpdatePhoto/UpdatePhoto';
import InlineError from '../../messages/InlineError';

import './EmployeeEdit.scss';

class EmployeeEdit extends Component {
  state = {
    employee: {
      id: this.props.id,
      name: this.props.name,
      surName: this.props.surName,
      avatar: this.props.avatar,
      email: this.props.email,
      birthday: formatDate(this.props.birthday),
      position: this.props.position,
      location: this.props.location,
      locationId: this.props.locationId,
      positionId: this.props.positionId
    },
    loading: false,
    errors: {}
  }

  componentDidMount () {
    this.props.dispatch(getLocations());
    this.props.dispatch(getPositions());
  }

  onChange = event =>
    this.setState({
      employee: { ...this.state.employee, [event.target.name]: event.target.value }
    });

  onChangeImage = event => {
    this.setState({ loading: true });
    const avatar = event.target.files[0];
    const reader = new FileReader();

    reader.readAsDataURL(avatar);
    reader.onload = () => {
      const { result } = reader;

      this.setState({ employee: { ...this.state.employee, avatar: result } });
      this.setState({ loading: false });
    };
  }

  handleChangeDate = (event, { name, value }) => {
    if (this.state.employee.hasOwnProperty(name)) {
      this.setState({ employee: { ...this.state.employee, [name]: value } });
    }
  }

  handleChangeLocation = (value) => {
    this.setState({ employee: { ...this.state.employee, locationId: value } });
  }

  handleChangePosition = (value) => {
    this.setState({ employee: { ...this.state.employee, positionId: value } });
  }

  onSubmit = (event) => {
    event.preventDefault();

    const errors = this.validate(this.state.employee);

    this.setState({ errors });
    if (Object.keys(errors).length === 0) {
      const { birthday } = this.state.employee;

      this.handleClose();
      this.props.updateEmployee({ ...this.state.employee, birthday: convertDate(birthday) });
      this.props.idUpdate(this.state.employee.id);
    }    
  };

  handleClose = () => this.props.handleClose();

  validate = data => {
    const errors = {};

    if (!Validator.isEmail(data.email)) errors.email = 'Enter email';
    if (!data.name) errors.name = 'Enter name';
    if (!data.surName) errors.surName = 'Enter Surname';
    return errors;
  }

  render () {
    const { locations, positions } = this.props;
    const { name, surName, birthday, location, position, email, avatar } = this.state.employee;
    const { loading, errors } = this.state;
  
    return (
      <Fragment>
        <Modal.Header>Edit employee {this.props.name} {this.props.surName}</Modal.Header>
        <Modal.Content image>
          <Form className='EmployeeEdit-form' onSubmit={this.onSubmit}>

            <div className='EmployeeEdit-form__photo'>
              {loading && <Loader active />}
              <Image 
                wrapped 
                size='medium' 
                src={
                  avatar 
                    ? avatar 
                    : 'https://tracker.moodle.org/secure/attachment/30912/f3.png'
                } />
              <UpdatePhoto updatePhoto={this.onChangeImage} />
            </div>

            <Modal.Description className='EmployeeEdit-form__description'>
              <Form.Field error={!!errors.name}>
                <label htmlFor="name"></label>
                <Form.Input 
                  fluid 
                  icon="user"
                  iconPosition="left"
                  name="name"
                  placeholder='Name'
                  value={name}
                  onChange={this.onChange} />
                {errors.name && <InlineError text={errors.name} />}
              </Form.Field>

              <Form.Field error={!!errors.surName}>
                <label htmlFor="surName"></label>
                <Form.Input 
                  fluid 
                  icon="user"
                  iconPosition="left"
                  name="surName"
                  placeholder='Surname'
                  value={surName}
                  onChange={this.onChange} />
                {errors.surName && <InlineError text={errors.surName} />}
              </Form.Field>   

              <Form.Field>
                <label htmlFor="locationId"></label>
                <Select 
                  defaultValue={location !== undefined ? location.name : null} 
                  size='default'
                  id='locationId'
                  placeholder='Location'
                  style={{ width: 203.39, height: 38 }} 
                  onChange={this.handleChangeLocation}>
                  {locations.map((index) => 
                    <Select.Option key={index.id} value={index.id}>{index.name}</Select.Option>            
                  )}                
                </Select>
              </Form.Field>

              <Form.Field>
                <label htmlFor="positionId"></label>
                <Select 
                  defaultValue={position !== undefined ? position.name : null}
                  size='default'
                  id='positionId'
                  placeholder='Position'
                  style={{ width: 203.39, height: 38 }} 
                  onChange={this.handleChangePosition}>
                  {positions.map((index) => 
                    <Select.Option key={index.id} value={index.id}>{index.name}</Select.Option>            
                  )}                
                </Select>
              </Form.Field>              
            </Modal.Description>

            <Modal.Description className='EmployeeEdit-form__description'>
              <Form.Field>
                <label htmlFor="birthday"></label>
                <DateInput 
                  closable
                  id='birthday'
                  name='birthday'
                  iconPosition='left'
                  popupPosition='bottom center'
                  placeholder='Birthday'
                  dateFormat='DD.MM.YYYY'
                  value={birthday}
                  onChange={this.handleChangeDate} />
              </Form.Field>

              <Form.Field error={!!errors.email}>
                <label htmlFor="email"></label>
                <Form.Input 
                  fluid 
                  icon="mail"
                  iconPosition="left"
                  placeholder='Email'
                  name="email"
                  value={email}
                  onChange={this.onChange} />
                {errors.email && <InlineError text={errors.email} />}
              </Form.Field>

              <Button className='EmployeeEdit-form__button' color='teal' size='large'>
                <Icon name='save'/>Save
              </Button>
            </Modal.Description>            
          </Form>
        </Modal.Content>
      </Fragment>
    );
  }  
}

EmployeeEdit.propTypes = {
  dispatch: PropTypes.func.isRequired,
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  surName: PropTypes.string.isRequired,
  email: PropTypes.string.isRequired,
  avatar: PropTypes.string,
  birthday: PropTypes.string,
  position: PropTypes.object,
  location: PropTypes.object,
  updateEmployee: PropTypes.func.isRequired,
  handleClose: PropTypes.func.isRequired,
  locations: PropTypes.array.isRequired,
  locationId: PropTypes.string,
  positionId: PropTypes.string,
  positions: PropTypes.array.isRequired,
  idUpdate: PropTypes.func.isRequired
};

const mapStateToProps = (state) => ({
  locations: state.locations.locations,
  positions: state.positions.positions
});

export default connect(mapStateToProps)(EmployeeEdit);