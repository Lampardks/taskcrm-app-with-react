import React, { Component } from 'react';
import { Segment, Grid, Button, Icon, Modal, Loader, Header, Container } from 'semantic-ui-react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import EmployeesItem from '../../forms/EmployeesItem/EmployeesItem';
import EmployeeAdd from '../../modal/EmployeeAdd/EmployeeAdd';
import formatDate from '../../../utils/formatDate';

import { getEmployees, updateEmployee, removeEmployee } from '../../../redux/actions/employees';
import { getProjects, getProjectEmployees } from '../../../redux/actions/projects';

import './EmployeesPage.scss';

class Employees extends Component {

  state = {
    loading: false,
    loadingAll: false,
    modalOpen: false,
    isProjectEmployees: false,
    project: {
      creationDate: '',
      name: '',
      description: ''
    }
  }

  componentDidMount () {
    this.setState({ loadingAll: true });

    if (this.props.match.params.id) {
      this.props.dispatch(getProjects())
        .then(() => {
          const { id } = this.props.match.params;
          const { projects } = this.props;

          const project = projects.filter(index => {
            return index.id === id;
          });

          this.props.dispatch(getProjectEmployees(id))
            .then(() => {
              this.setState({ 
                isProjectEmployees: true, 
                loadingAll: false, 
                project: {
                  ...project[0]
                }
              });
            });
        });
    } else {
      this.props.dispatch(getEmployees())
        .then(() => 
          this.setState({ loadingAll: false })
        );
    }    
  }

  updateEmployee = employee => {
    this.setState({ loading: true });
    /* eslint-disable no-console */
    // console.log(employee);
    this.props.dispatch(updateEmployee(employee))
      .then(() => {
        this.setState({ loading: false });
      });
  }

  onRemove = id => {
    this.setState({ loadingAll: true });
    this.props.dispatch(removeEmployee(id))
      .then(() => {
        this.setState({ loadingAll: false });
      });
  }

  handleOpen = () => this.setState({ modalOpen: true });

  handleClose = () => this.setState({ modalOpen: false });

  render () {
    const { employees, userId, projects } = this.props;
    const { loading, loadingAll, isProjectEmployees, project  } = this.state;
    const employeesFilter = employees.filter(employee => {
      return userId !== employee.id;
    });

    /* eslint-disable no-console */
    // console.log(this.state);

    return (
      <Segment padded='very' className="Segment-padding">
        {loadingAll && <Loader active />}
        <Grid >
          <Grid.Row className='Employees-line'>
            {isProjectEmployees 
              ? <Container text className='Employees-line__project'>
                <Header as='h1'>{project.name}</Header>
                <span>{formatDate(project.creationDate)}</span>
                <p>{project.description}</p>
              </Container>
              : <Modal 
                centered={false}
                open={this.state.modalOpen}
                onClose={this.handleClose}
                trigger={
                  <Button 
                    onClick={this.handleOpen} 
                    className='Employees-line__button' 
                    color='green' 
                    size='small'>
                    <Icon name='add user'/>Add employee
                  </Button>
                }>
                <EmployeeAdd 
                  projects={projects}
                  handleClose={this.handleClose} />
              </Modal>
            }
          </Grid.Row>
          {employeesFilter.map(employee => 
            <EmployeesItem             
              key={employee.id}
              updateEmployee={this.updateEmployee}
              onRemove={this.onRemove}
              loading={loading}
              {...employee} />
          )} 
        </Grid>
      </Segment>
    );
  }
}

Employees.propTypes = {
  dispatch: PropTypes.func.isRequired,
  employees: PropTypes.array,
  projects: PropTypes.array,
  userId: PropTypes.string.isRequired,
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string
    }).isRequired
  }).isRequired
};

const mapStateToProps = state => ({
  employees: state.employees.employees,
  userId: state.user.user.id,
  projects: state.projects.projects
});

export default connect(mapStateToProps)(Employees);