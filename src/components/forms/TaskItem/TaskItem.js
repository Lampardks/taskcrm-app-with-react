import React, { Component } from 'react';
import { List } from 'semantic-ui-react';
import PropTypes from 'prop-types';

// import './ProjectsItem.scss';

class TaskItem extends Component {  
  render () {
    const { taskName } = this.props;

    /* eslint-disable no-console */
    // console.log(taskName);

    return (
      <List.Item className='Projects-list__item'>
        <List.Content>
          <List.Header>{taskName}</List.Header>
        </List.Content>
      </List.Item>
    );
  }  
}

TaskItem.propTypes = {
  taskName: PropTypes.string.isRequired
};

export default TaskItem;