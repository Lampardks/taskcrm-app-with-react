import axios from 'axios';

export default (token = null) => {
  if (token) {
    axios.defaults.headers['AuthToken']  = token;
    // axios.defaults.headers.withCredentials = true;
    /* eslint-disable no-console */
    // console.log(axios.defaults);
  } else {
    delete axios.defaults.headers['AuthToken'];
  }
};