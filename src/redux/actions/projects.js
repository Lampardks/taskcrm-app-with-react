import axios from 'axios';
import { projectsUrl, projectEmployeesUrl } from '../../urls';

export const getProjects = () => dispatch => {
  return axios.get(projectsUrl())
    .then(({ data }) => {
      dispatch({
        type: 'GET_PROJECTS',
        payload: data
      });
    });
};

export const getProjectEmployees = id => dispatch => {
  return axios.get(projectEmployeesUrl(id))
    .then(({ data }) => 
      dispatch({
        type: 'GET_EMPLOYEES',
        payload: data
      })
    );
};

export const addProject = project => dispatch => {
  return axios.post(projectsUrl(), project)
    .then(() => {
      dispatch(getProjects());
    });
};