import React, { Component } from 'react';
import { Segment, Loader, Header } from 'semantic-ui-react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import TasksContainer from '../../dnd/TasksContainer';
import TaskAdd from '../../modal/TaskAdd/TaskAdd';

import { 
  getProjectTasks, 
  getStatuses, 
  moveCard, 
  getTypeTask, 
  addTask,
  onRemove,
  updateTask
} from '../../../redux/actions/tasks';
import { getEmployees } from '../../../redux/actions/employees';
import { getProjects } from '../../../redux/actions/projects';

import './TasksPage.scss';

class TasksPage extends Component {
  state = {
    isLoading: false,
    modalOpen: false,
    project: {
      id: '',
      name: ''
    },
    typeTask: []
  }

  componentDidMount () {
    this.setState({ isLoading: true });
    this.props.dispatch(getEmployees())       
      .then(() => {
        this.props.dispatch(getTypeTask());
        this.props.dispatch(getStatuses())
          .then(() => {
            this.props.dispatch(getProjects())
              .then(() => {
                const { id } = this.props.match.params;
                const { projects } = this.props;

                const project = projects.filter(index => {
                  return index.id === id;
                });

                this.props.dispatch(getProjectTasks(this.props.match.params.id))          
                  .then(() => this.setState({ 
                    isLoading: false,
                    project: {
                      ...project[0]
                    }
                  }));
              });
          });          
      });
  }

  moveCard = (id, lastX, lastY, nextX, nextY) => {
    this.props.dispatch(moveCard(
      this.props.match.params.id, 
      id, 
      lastX, 
      lastY, 
      nextX, 
      nextY
    ));
  }

  addTask = task => {
    this.props.dispatch(addTask(task));
  }

  onRemove = id => {
    this.props.dispatch(onRemove(id, this.props.match.params.id));
  }

  updateTask = task => {
    this.props.dispatch(updateTask(task));
  }

  render () {
    const { tasks, statuses, employees, typeTask } = this.props;
    const { isLoading, project } = this.state;

    return (
      <Segment className="Segment-padding Segment-padding__tasks-page">
        {isLoading && <Loader active />}
        <div className='Task-page__header'>
          <Header as='h1' color='teal' textAlign='center'>
            {project.name}
          </Header>
          <TaskAdd 
            employees={employees}
            project={project}
            addTask={this.addTask} />
        </div>
        <TasksContainer 
          tasks={tasks} 
          statuses={statuses}
          employees={employees}
          typeTask={typeTask}
          moveCard={this.moveCard}
          onRemove={this.onRemove}
          updateTask={this.updateTask} />
      </Segment>
    );
  }
}

TasksPage.propTypes = {
  dispatch: PropTypes.func.isRequired,
  tasks: PropTypes.array.isRequired,
  employees: PropTypes.array.isRequired,
  projects: PropTypes.array.isRequired,
  typeTask: PropTypes.array,
  statuses: PropTypes.array.isRequired,
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string
    }).isRequired
  }).isRequired
};

const mapStateToProps = state => ({
  tasks: state.tasks.tasks,
  employees: state.employees.employees,
  typeTask: state.typeTask.typeTask,
  statuses: state.statuses.statuses,
  projects: state.projects.projects
});

export default connect(mapStateToProps)(TasksPage);
