import React, { Component, Fragment } from 'react';
import { Image, Modal, Form, Button, Icon, Loader } from 'semantic-ui-react';
import { Select } from 'antd';
import { DateInput } from 'semantic-ui-calendar-react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Validator from 'validator';

import { getLocations, getPositions, addEmployee } from '../../../redux/actions/employees';

import formatDate from '../../../utils/formatDate';
import convertDate from '../../../utils/convertDate';
import UpdatePhoto from '../../forms/UpdatePhoto/UpdatePhoto';
import InlineError from '../../messages/InlineError';

import './EmployeeAdd.scss';

class EmployeeAdd extends Component {
  state = {
    employee: {
      name: '',
      surName: '',
      avatar: 'https://tracker.moodle.org/secure/attachment/30912/f3.png',
      email: '',
      password: '',
      birthday: formatDate(new Date()),
      locationId: '',
      positionId: ''
    },
    // projectId: '',
    confirmPassword: '',
    loading: false,
    errors: {}
  }

  componentDidMount () {
    this.props.dispatch(getLocations());
    this.props.dispatch(getPositions());
  }

  onChange = event =>
    this.setState({
      employee: { ...this.state.employee, [event.target.name]: event.target.value }
    });

  onChangeConfPass = event =>
    this.setState({
      ...this.state, [event.target.name]: event.target.value
    });

  onChangeImage = event => {
    this.setState({ loading: true });
    const avatar = event.target.files[0];
    const reader = new FileReader();

    reader.readAsDataURL(avatar);
    reader.onload = () => {
      const { result } = reader;

      this.setState({ employee: { ...this.state.employee, avatar: result } });
      this.setState({ loading: false });
    };
  }

  handleChangeDate = (event, { name, value }) => {
    if (this.state.employee.hasOwnProperty(name)) {
      this.setState({ employee: { ...this.state.employee, [name]: value } });
    }
  }

  handleChangeLocation = (value) => {
    this.setState({ employee: { ...this.state.employee, locationId: value } });
  }

  handleChangePosition = (value) => {
    this.setState({ employee: { ...this.state.employee, positionId: value } });
  }

  // handleChangeProject = (value) => {
  //   /* eslint-disable no-console */
  //   console.log(value);
  //   this.setState({ projectId: value });
  // }

  onSubmit = (event) => {
    event.preventDefault();

    const errors = this.validate(this.state);

    this.setState({ errors });
    if (Object.keys(errors).length === 0) {
      const { birthday } = this.state.employee;

      /* eslint-disable no-console */
      console.log(this.state.projectId);

      this.handleClose();
      this.props.dispatch(addEmployee({ 
        ...this.state.employee, birthday: convertDate(birthday) 
      }
      ));
    }    
  };

  handleClose = () => this.props.handleClose();

  validate = data => {
    const { email, name, surName, password } = data.employee;
    const errors = {};

    if (!Validator.isEmail(email)) errors.email = 'Enter email';
    if (!name) errors.name = 'Enter name';
    if (!surName) errors.surName = 'Enter Surname';
    if (!password) errors.password = 'Enter password';
    if (password !== data.confirmPassword) errors.password = 'Passwords must match';
    return errors;
  }

  render () {
    const { locations, positions } = this.props;
    const { name, surName, birthday, email, avatar } = this.state.employee;
    const { loading, errors } = this.state;

    /* eslint-disable no-console */
    // console.log(projects);
  
    return (
      <Fragment>
        <Modal.Header>Add new employee</Modal.Header>
        <Modal.Content image>
          <Form className='EmployeeAdd-form' onSubmit={this.onSubmit}>

            <div className='EmployeeAdd-form__photo'>
              {loading && <Loader active />}
              <Image 
                wrapped 
                size='medium' 
                src={avatar} />
              <UpdatePhoto updatePhoto={this.onChangeImage} />
            </div>

            <Modal.Description className='EmployeeAdd-form__description'>
              <Form.Field error={!!errors.name}>
                <label htmlFor="name"></label>
                <Form.Input 
                  fluid 
                  icon="user"
                  iconPosition="left"
                  name="name"
                  placeholder='Name'
                  value={name}
                  onChange={this.onChange} />
                {errors.name && <InlineError text={errors.name} />}
              </Form.Field>

              <Form.Field error={!!errors.surName}>
                <label htmlFor="surName"></label>
                <Form.Input 
                  fluid 
                  icon="user"
                  iconPosition="left"
                  name="surName"
                  placeholder='Surname'
                  value={surName}
                  onChange={this.onChange} />
                {errors.surName && <InlineError text={errors.surName} />}
              </Form.Field>   

              <Form.Field>
                <label htmlFor="locationId"></label>
                <Select 
                  size='default'
                  id='locationId'
                  placeholder='Location'
                  style={{ width: 203.39, height: 38 }} 
                  onChange={this.handleChangeLocation}>
                  {locations.map((index) => 
                    <Select.Option key={index.id} value={index.id}>{index.name}</Select.Option>            
                  )}                
                </Select>
              </Form.Field>

              <Form.Field>
                <label htmlFor="positionId"></label>
                <Select 
                  size='default'
                  id='positionId'
                  placeholder='Position'
                  style={{ width: 203.39, height: 38 }} 
                  onChange={this.handleChangePosition}>
                  {positions.map((index) => 
                    <Select.Option key={index.id} value={index.id}>{index.name}</Select.Option>            
                  )}                
                </Select>
              </Form.Field>           
            </Modal.Description>

            <Modal.Description className='EmployeeAdd-form__description'>
              <Form.Field>
                <label htmlFor="birthday"></label>
                <DateInput 
                  closable
                  id='birthday'
                  name='birthday'
                  iconPosition='left'
                  popupPosition='bottom center'
                  placeholder='Birthday'
                  dateFormat='DD.MM.YYYY'
                  value={birthday}
                  onChange={this.handleChangeDate} />
              </Form.Field>

              <Form.Field error={!!errors.email}>
                <label htmlFor="email"></label>
                <Form.Input 
                  fluid 
                  icon="mail"
                  iconPosition="left"
                  placeholder='Email'
                  name="email"
                  value={email}
                  onChange={this.onChange} />
                {errors.email && <InlineError text={errors.email} />}
              </Form.Field>

              <Form.Field error={!!errors.password}>
                <label htmlFor="password"></label>
                <Form.Input 
                  fluid 
                  icon='lock'
                  type='password'
                  iconPosition='left'
                  placeholder='Password'
                  name='password'
                  onChange={this.onChange} />
                {errors.password && <InlineError text={errors.password} />}
              </Form.Field>

              <Form.Field error={!!errors.password}>
                <label htmlFor="confirmPassword"></label>
                <Form.Input 
                  fluid 
                  icon='lock'
                  type='password'
                  iconPosition='left'
                  placeholder='Confirm password'
                  name='confirmPassword'
                  onChange={this.onChangeConfPass} />
                {errors.password && <InlineError text={errors.password} />}
              </Form.Field>

              <Button className='EmployeeAdd-form__button' color='teal' size='large'>
                <Icon name='save'/>Save
              </Button>
            </Modal.Description>            
          </Form>
        </Modal.Content>
      </Fragment>
    );
  }  
}

EmployeeAdd.propTypes = {
  dispatch: PropTypes.func.isRequired,
  handleClose: PropTypes.func.isRequired,
  locations: PropTypes.array.isRequired,
  positions: PropTypes.array.isRequired,
  projects: PropTypes.array
};

const mapStateToProps = (state) => ({
  locations: state.locations.locations,
  positions: state.positions.positions
});

export default connect(mapStateToProps)(EmployeeAdd);