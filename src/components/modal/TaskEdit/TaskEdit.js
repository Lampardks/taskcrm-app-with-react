import React, { Component } from 'react';
import { Modal, Form, Button, Icon, TextArea } from 'semantic-ui-react';
import { Select } from 'antd';
import PropTypes from 'prop-types';

import InlineError from '../../messages/InlineError';

class TaskAdd extends Component {
  state = {
    task: {
      id: this.props.task.id,
      employeeId: this.props.task.employeeId,
      projectId: this.props.task.projectId,
      projectName: this.props.task.projectName,
      name: this.props.task.name,
      description: this.props.task.description,
      statusId: this.props.task.statusId,
      sortId: this.props.task.sortId,
      typeTaskId: this.props.task.typeTaskId
    },
    modalOpen: false,
    errors: {}
  }

  onChange = event =>
    this.setState({
      task: { ...this.state.task, [event.target.name]: event.target.value }
    });

  handleChangeTask = (value) => {
    this.setState({ task: { ...this.state.task, typeTaskId: value } });
  }

  handleChangeEmployee = (value) => {
    this.setState({ task: { ...this.state.task, employeeId: value } });
  }

  onSubmit = (event) => {
    event.preventDefault();

    const errors = this.validate(this.state);

    this.setState({ errors });
    if (Object.keys(errors).length === 0) {
      this.handleClose();
      this.props.updateTask({ 
        ...this.state.task
      });
    }    
  };

  handleOpen = () => this.setState({ modalOpen: true });

  handleClose = () => this.setState({ modalOpen: false });

  validate = data => {
    const { name, description } = data.task;
    const errors = {};

    if (!name) errors.name = 'Enter task name';
    if (!description) errors.surName = 'Enter description';
    return errors;
  }

  render () {
    const { errors, modalOpen } = this.state;
    const { name, description } = this.state.task;
    const { typeTasks, typeTaskName, employees, employee } = this.props;
  
    return (
      <Modal 
        centered={false}
        open={modalOpen}
        onClose={this.handleClose}
        trigger={
          <a 
            onClick={this.handleOpen} 
            className='Edit-task__button' 
            title='Edit task'>
            <Icon name='edit' />
          </a>
        }>
        <Modal.Header>Edit task</Modal.Header>
        <Modal.Content image>
          <Form className='' onSubmit={this.onSubmit}>
            <div className='Add-task-container'>
              <Modal.Description className='Add-task-container__item'>
                <Form.Field error={!!errors.name}>
                  <label htmlFor="name"></label>
                  <Form.Input 
                    fluid 
                    name="name"
                    placeholder='Name task'
                    value={name}
                    onChange={this.onChange} />
                  {errors.name && <InlineError text={errors.name} />}
                </Form.Field>               

                <Form.Field>
                  <label htmlFor="typeTaskId"></label>
                  <Select 
                    defaultValue={typeTaskName !== undefined ? typeTaskName : null} 
                    size='default'
                    id='typeTaskId'
                    placeholder='Type task'
                    style={{ width: 203.39, height: 38 }} 
                    onChange={this.handleChangeTask}>
                    {typeTasks.map((index) => 
                      <Select.Option key={index.id} value={index.id}>{index.name}</Select.Option>            
                    )}                
                  </Select>
                </Form.Field>

                <Form.Field>
                  <label htmlFor="employeeId"></label>
                  <Select 
                    defaultValue={employee !== undefined ? employee.name + ' ' + employee.surName : null} 
                    size='default'
                    id='employeeId'
                    placeholder='Responsible employee'
                    style={{ width: 203.39, height: 38 }} 
                    onChange={this.handleChangeEmployee}>
                    {employees.map((index) => 
                      <Select.Option key={index.id} value={index.id}>{index.name} {index.surName}</Select.Option>            
                    )}                
                  </Select>
                </Form.Field>
                                
              </Modal.Description>             
            
              <Modal.Description className='Add-task-container__item'>
                <Form.Field error={!!errors.description}>
                  <label htmlFor="description"></label>
                  <TextArea 
                    name="description"
                    id='description'
                    placeholder='Description'
                    value={description}
                    style={{ minHeight: 142, minWidth: 245 }}
                    onChange={this.onChange} />
                  {errors.description && <InlineError text={errors.description} />}
                </Form.Field>
              </Modal.Description>
            </div>
            <div className='Add-task__button-popup'>
              <Button className='Add-task__button-popup__button' color='teal' size='large'>
                <Icon name='save'/>Save
              </Button>
            </div>        
          </Form>
        </Modal.Content>
      </Modal>
    );
  }  
}

TaskAdd.propTypes = {
  task: PropTypes.object.isRequired,
  typeTask: PropTypes.array.isRequired,
  typeTasks: PropTypes.array.isRequired,
  typeTaskName: PropTypes.string.isRequired,
  employees: PropTypes.array.isRequired,
  employee: PropTypes.object.isRequired,
  updateTask: PropTypes.func.isRequired
};

export default TaskAdd;