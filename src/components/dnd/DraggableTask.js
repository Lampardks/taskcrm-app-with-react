import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { findDOMNode } from 'react-dom';
import { DragSource } from 'react-dnd';
import { getEmptyImage } from 'react-dnd-html5-backend';

import Task from './Task';


function getStyles (isDragging) {
  return {
    display: isDragging ? 0.5 : 1
  };
}

const cardSource = {
  beginDrag (props, monitor, component) {
    // dispatch to redux store that drag is started
    const { task, x, y } = props;
    const { id, name } = task;

    /* eslint-disable react/no-find-dom-node */
    const { clientWidth, clientHeight } = findDOMNode(component);
    /* eslint-enable react/no-find-dom-node */

    return { id, name, task, x, y, clientWidth, clientHeight };
  },
  endDrag (props, monitor) {
    document.getElementById(monitor.getItem().id).style.display = 'block';
  },
  isDragging (props, monitor) {
    const isDragging = props.task && props.task.id === monitor.getItem().id;

    return isDragging;
  }
};

// options: 4rd param to DragSource https://gaearon.github.io/react-dnd/docs-drag-source.html
const OPTIONS = {
  arePropsEqual: function arePropsEqual (props, otherProps) {
    let isEqual = true;

    if (props.task.id === otherProps.task.id &&
        props.x === otherProps.x &&
        props.y === otherProps.y
    ) {
      isEqual = true;
    } else {
      isEqual = false;
    }
    return isEqual;
  }
};

function collectDragSource (connectDragSource, monitor) {
  return {
    connectDragSource: connectDragSource.dragSource(),
    connectDragPreview: connectDragSource.dragPreview(),
    isDragging: monitor.isDragging()
  };
}

class DraggableTask extends Component {
  state = {
    deleteOpen: false
  }

  componentDidMount () {
    this.props.connectDragPreview(getEmptyImage(), {
      captureDraggingState: true
    });
  }

  handleOpenDel = () => this.setState({ deleteOpen: true });
  handleCloseDel = () => this.setState({ deleteOpen: false });

  onRemove = () => {
    this.handleCloseDel();
    this.props.onRemove(this.props.task.id);
  }

  render () {
    const { 
      connectDragSource, 
      task, 
      employee, 
      isDragging, 
      typeTask,
      typeTasks,
      employees,
      updateTask
    } = this.props;
    const { deleteOpen, modalOpen } = this.state;

    /* eslint-disable no-console */
    // console.log(this.props);

    return connectDragSource(
      <div>
        <Task 
          style={getStyles(isDragging)} 
          task={task} 
          typeTask={typeTask}
          employee={employee}
          deleteOpen={deleteOpen}
          handleOpenDel={this.handleOpenDel}
          handleCloseDel={this.handleCloseDel}
          onRemove={this.onRemove}
          modalOpen={modalOpen}
          typeTasks={typeTasks}
          employees={employees}
          updateTask={updateTask} />
      </div>
    );
  }
}

DraggableTask.propTypes = {
  task: PropTypes.object,
  employee: PropTypes.object,
  typeTask: PropTypes.object,
  connectDragSource: PropTypes.func.isRequired,
  connectDragPreview: PropTypes.func.isRequired,
  isDragging: PropTypes.bool.isRequired,
  x: PropTypes.number.isRequired,
  y: PropTypes.number,
  onRemove: PropTypes.func.isRequired,
  typeTasks: PropTypes.array.isRequired,
  employees: PropTypes.array.isRequired,
  updateTask: PropTypes.func.isRequired
};

export default DragSource('task', cardSource, collectDragSource, OPTIONS)(DraggableTask);
