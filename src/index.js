import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import store from './Redux/store';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import App from './App';
import { userLoggedIn } from './redux/actions/auth';
import setAuthorizationHeader from './utils/setAuthorizationHeader';

import 'semantic-ui-css/semantic.min.css';
import 'semantic-ui-calendar-react/dist/css/calendar.min.css';
import 'antd/dist/antd.css';
import './styles.scss';

if (localStorage.taskCRMToken) {
  const user = {
    isAuthenticated: true,
    user: JSON.parse(localStorage.getItem('taskCRMUser'))
  };
  
  setAuthorizationHeader(localStorage.taskCRMToken);
  store.dispatch(userLoggedIn(user));
}

ReactDOM.render(
  <Router>
    <Provider store={store}>
      <Route component={App} />
    </Provider>
  </Router>,
  document.getElementById('root')
);