const initialState =  {
  typeTask: []
};

const typeTask = (state = initialState, action) => {
  switch (action.type) {
    case 'GET_TYPE_TASK':
      return { ...state, typeTask: action.payload };
    default:
      return state;
  }
};

export default typeTask;