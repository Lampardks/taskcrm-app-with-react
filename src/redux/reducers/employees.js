const initialState =  {
  employees: []
};

const employees = (state = initialState, action) => {
  switch (action.type) {
    case 'GET_EMPLOYEES':
      return { ...state, employees: action.payload };
    case 'UPDATED_EMPLOYEE': {
      const { employees } = state;
      const { payload } = action;

      for (let i = 0; i < employees.length; i++) {
        if (employees[i].id === payload.id) {
          employees[i] = payload;
          break;
        }
      }
      return { ...state, employees };
    }
    default:
      return state;
  }
};

export default employees;