import React from 'react';
import { hot } from 'react-hot-loader';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Route } from 'react-router-dom';
import ProfileUser from './components/pages/ProfileUser/ProfileUser';
import AuthorizationPage from './components/pages/AuthorizationPage/AuthorizationPage';
import EmployeesPage from './components/pages/EmployeesPage/EmployeesPage';
import ProjectsPage from './components/pages/ProjectsPage/ProjectsPage';
import TasksPage from './components/pages/TasksPage/TasksPage';
import TopNavigation from './components/navigation/TopNavigation';

import UserRoute from './components/routes/UserRoute';
import GuestRoute from './components/routes/GuestRoute';

import './App.scss';

const App = ({ isAuthenticated, location }) => {

  /* eslint-disable no-console */
  // console.log(isAuthenticated);
  /* eslint-enable no-console */

  return (
    <div className='ui container'>
      {isAuthenticated && <TopNavigation />}
      <Route location={location} path='/' exact component={ProfileUser} />
      <GuestRoute location={location} path='/auth' exact component={AuthorizationPage} />
      <UserRoute location={location} path='/employees' exact component={EmployeesPage} />
      <UserRoute location={location} path='/projects' exact component={ProjectsPage} />
      <UserRoute location={location} path='/projects/:id/tasks' exact component={TasksPage} />
      <UserRoute location={location} path='/projects/:id/employees' exact component={EmployeesPage} />
    </div>
  );
  
};

App.propTypes = {
  location: PropTypes.shape({
    pathname: PropTypes.string.isRequired
  }).isRequired,
  isAuthenticated: PropTypes.bool.isRequired
};

const mapStateToProps = (state) => ({
  isAuthenticated: !!state.user.isAuthenticated
});

export default hot(module)(connect(mapStateToProps)(App));
