import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Grid, Header } from 'semantic-ui-react';
import LoginForm from '../../forms/LoginForm';
import { login } from '../../../redux/actions/auth';

import './AuthorizationPage.scss';

class AuthorizationPage extends Component {
  submit = data => 
    this.props.login(data)
      .then(() => this.props.history.push('/'));

  render () {
    return (
      <div className='login-form'>
        <Grid textAlign='center' style={{ height: '100%' }} verticalAlign='middle'>
          <Grid.Column style={{ maxWidth: 450 }}>
            <Header as='h1' color='teal' textAlign='center'>
              Login
            </Header>
            <LoginForm submit={this.submit} />
          </Grid.Column>
        </Grid>
      </div>
    );
  }
}

AuthorizationPage.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func.isRequired
  }).isRequired,
  login: PropTypes.func.isRequired
};

export default connect(null, { login })(AuthorizationPage);