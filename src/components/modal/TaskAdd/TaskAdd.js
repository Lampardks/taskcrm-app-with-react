import React, { Component } from 'react';
import { Modal, Form, Button, Icon, TextArea } from 'semantic-ui-react';
import { Select } from 'antd';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import InlineError from '../../messages/InlineError';

import { getTypeTask } from '../../../redux/actions/tasks';

import './TaskAdd.scss';

class TaskAdd extends Component {
  state = {
    task: {
      employeeId: '',
      projectId: '',
      projectName: '',
      name: '',
      description: '',
      statusId: 0,
      sortId: 0,
      typeTaskId: ''
    },
    modalOpen: false,
    loading: false,
    errors: {}
  }

  componentDidMount () {
    this.props.dispatch(getTypeTask());
  }

  onChange = event =>
    this.setState({
      task: { ...this.state.task, [event.target.name]: event.target.value }
    });

  handleChangeTask = (value) => {
    this.setState({ task: { ...this.state.task, typeTaskId: value } });
  }

  handleChangeEmployee = (value) => {
    this.setState({ task: { ...this.state.task, employeeId: value } });
  }

  onSubmit = (event) => {
    event.preventDefault();

    const errors = this.validate(this.state);

    this.setState({ errors });
    if (Object.keys(errors).length === 0) {
      this.handleClose();
      this.props.addTask({ 
        ...this.state.task, 
        projectId: this.props.project.id,
        projectName: this.props.project.name
      });
    }    
  };

  handleOpen = () => this.setState({ modalOpen: true });

  handleClose = () => this.setState({ modalOpen: false });

  validate = data => {
    const { name, description } = data.task;
    const errors = {};

    if (!name) errors.name = 'Enter task name';
    if (!description) errors.surName = 'Enter description';
    return errors;
  }

  render () {
    const { errors, modalOpen } = this.state;
    const { typeTask, employees } = this.props;
  
    return (
      <Modal 
        centered={false}
        open={modalOpen}
        onClose={this.handleClose}
        trigger={
          <Button 
            onClick={this.handleOpen} 
            className='Add-task__button' 
            color='green' 
            size='small'>
            <Icon name='add circle'/>Add new task
          </Button>
        }>
        <Modal.Header>Add new task</Modal.Header>
        <Modal.Content image>
          <Form className='' onSubmit={this.onSubmit}>
            <div className='Add-task-container'>
              <Modal.Description className='Add-task-container__item'>
                <Form.Field error={!!errors.name}>
                  <label htmlFor="name"></label>
                  <Form.Input 
                    fluid 
                    name="name"
                    placeholder='Name task'
                    onChange={this.onChange} />
                  {errors.name && <InlineError text={errors.name} />}
                </Form.Field>               

                <Form.Field>
                  <label htmlFor="typeTaskId"></label>
                  <Select 
                    size='default'
                    id='typeTaskId'
                    placeholder='Type task'
                    style={{ width: 203.39, height: 38 }} 
                    onChange={this.handleChangeTask}>
                    {typeTask.map((index) => 
                      <Select.Option key={index.id} value={index.id}>{index.name}</Select.Option>            
                    )}                
                  </Select>
                </Form.Field>              
                <Form.Field>
                  <label htmlFor="employeeId"></label>
                  <Select 
                    size='default'
                    id='employeeId'
                    placeholder='Responsible employee'
                    style={{ width: 203.39, height: 38 }} 
                    onChange={this.handleChangeEmployee}>
                    {employees.map((index) => 
                      <Select.Option key={index.id} value={index.id}>{index.name} {index.surName}</Select.Option>            
                    )}                
                  </Select>
                </Form.Field>
              </Modal.Description>             
            
              <Modal.Description className='Add-task-container__item'>
                <Form.Field error={!!errors.description}>
                  <label htmlFor="description"></label>
                  <TextArea 
                    name="description"
                    id='description'
                    placeholder='Description'
                    style={{ minHeight: 142, minWidth: 245 }}
                    onChange={this.onChange} />
                  {errors.description && <InlineError text={errors.description} />}
                </Form.Field>
              </Modal.Description>
            </div>
            <div className='Add-task__button-popup'>
              <Button className='Add-task__button-popup__button' color='teal' size='large'>
                <Icon name='save'/>Save
              </Button>
            </div>        
          </Form>
        </Modal.Content>
      </Modal>
    );
  }  
}

TaskAdd.propTypes = {
  dispatch: PropTypes.func.isRequired,
  typeTask: PropTypes.array,
  employees: PropTypes.array.isRequired,
  project: PropTypes.object,
  addTask: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  typeTask: state.typeTask.typeTask
});

export default connect(mapStateToProps)(TaskAdd);