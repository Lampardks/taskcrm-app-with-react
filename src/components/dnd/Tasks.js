import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { DropTarget } from 'react-dnd';
import { findDOMNode } from 'react-dom';

import DraggableTask from './DraggableTask';

const getPlaceholderIndex = (y, scrollY) => {
  // shift placeholder if y position more than card height / 2
  const yPos = y - 84 + scrollY;
  let placeholderIndex;

  if (yPos < 161 / 2) {
    placeholderIndex = -1; // place at the start
  } else {
    placeholderIndex = Math.floor((yPos - 161 / 2) / (161 + 10));
  }
  return placeholderIndex;
};

const specs = {
  drop (props, monitor, component) {

    document.getElementById(monitor.getItem().id).style.display = 'block';
    const { placeholderIndex } = component.state;
    const lastX = monitor.getItem().x;
    const lastY = monitor.getItem().y;
    const nextX = props.x;
    let nextY = placeholderIndex;

    if (lastY > nextY) { // move top
      nextY += 1;
    } else if (lastX !== nextX) { // insert into another list
      nextY += 1;
    }

    if (lastX === nextX && lastY === nextY) { // if position equel
      return;
    }

    props.moveCard(document.getElementById(monitor.getItem().id).id, lastX, lastY, nextX, nextY);
  },
  hover (props, monitor, component) {

    // defines where placeholder is rendered
    /* eslint-disable react/no-find-dom-node */
    const placeholderIndex = getPlaceholderIndex(
      monitor.getClientOffset().y,
      findDOMNode(component).scrollTop
    );
    /* eslint-enable react/no-find-dom-node */

    // IMPORTANT!
    // HACK! Since there is an open bug in react-dnd, making it impossible
    // to get the current client offset through the collect function as the
    // user moves the mouse, we do this awful hack and set the state (!!)
    // on the component from here outside the component.
    // https://github.com/gaearon/react-dnd/issues/179
    component.setState({ placeholderIndex });

    // when drag begins, we hide the card and only display cardDragPreview
    const item = monitor.getItem();

    document.getElementById(item.id).style.display = 'none';
  }
};

class Tasks extends Component {

  constructor (props) {
    super(props);
    this.state = {
      placeholderIndex: undefined,
      isScrolling: false,
    };
  }

  render () {
    const { 
      connectDropTarget, 
      x, 
      canDrop, 
      isOver, 
      tasks, 
      employees, 
      statusId, 
      typeTask,
      onRemove,
      updateTask
    } = this.props;
    const { placeholderIndex } = this.state;

    let isPlaceHold = false;
    let taskList = [];

    /* eslint-disable no-console */
    // console.log(this.props);

    const countStatusTask = tasks.filter((task) => {
      return task.statusId === statusId;
    });

    tasks.forEach((task, i) => {
      const employee = employees.filter(employee => {
        return employee.id === task.employeeId;
      });

      const typeThisTask = typeTask.filter(type => {
        return type.id === task.typeTaskId;
      });

      if (isOver && canDrop) {
        isPlaceHold = false;
        if (i === 0 && placeholderIndex === -1 && countStatusTask.length !== 0) {
          taskList.push(<div key="placeholder" className="task-container placeholder" />);
        } else if (placeholderIndex > countStatusTask.length && countStatusTask.length !== 0) {
          isPlaceHold = true;
        }
      }
      if (task !== undefined && task.statusId === statusId) {
        taskList.push(
          <DraggableTask 
            x={x}
            y={task.sortId}
            task={task}
            employee={employee[0]}
            employees={employees}
            typeTask={typeThisTask[0]}
            typeTasks={typeTask}
            onRemove={onRemove}
            updateTask={updateTask}
            key={task.id} />
        );
      }
      if (
        isOver 
        && canDrop 
        && placeholderIndex === task.sortId 
        && task.statusId === statusId
      ) {
        taskList.push(<div key="placeholder" className="task-container placeholder" />);
      }
    });

    // if placeholder index is greater than array.length, display placeholder as last
    if (isPlaceHold) {
      taskList.push(<div key="placeholder" className="task-container placeholder" />);
    }

    // if there is no items in cards currently, display a placeholder anyway
    if (isOver && canDrop && countStatusTask.length === 0) {
      taskList.push(<div key="placeholder" className="task-container placeholder" />);
    }

    return connectDropTarget(
      <div className="task-items">
        {taskList}
      </div>
    );
  }
}

Tasks.propTypes = {
  tasks: PropTypes.array.isRequired,
  employees: PropTypes.array.isRequired,
  typeTask: PropTypes.array,
  statusId: PropTypes.number.isRequired,
  connectDropTarget: PropTypes.func.isRequired,
  x: PropTypes.number,
  canDrop: PropTypes.bool,
  isOver: PropTypes.bool,
  onRemove: PropTypes.func.isRequired,
  updateTask: PropTypes.func.isRequired
};

export default 
DropTarget('task', specs, (connect, monitor) => ({
  connectDropTarget: connect.dropTarget(),
  isOver: monitor.isOver(),
  canDrop: monitor.canDrop(),
  item: monitor.getItem()
}))(Tasks);
