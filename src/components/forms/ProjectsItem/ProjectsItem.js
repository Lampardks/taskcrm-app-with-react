import React, { Component } from 'react';
import { List, Button, Icon } from 'semantic-ui-react';
import PropTypes from 'prop-types';

import './ProjectsItem.scss';

class ProjectsItem extends Component {  

  // state = {
  //   modalOpen: false,
  //   deleteOpen: false,
  //   isLoading: false
  // }

  tasksProject = () => {
    const { id } = this.props;

    /* eslint-disable no-console */
    // console.log(this);

    this.context.router.history.push(`/projects/${id}/tasks`);
  }

  teamProject = () => {
    const { id } = this.props;

    this.context.router.history.push(`/projects/${id}/employees`);
  }

  // handleOpen = () => this.setState({ modalOpen: true });
  // handleOpenDel = () => this.setState({ deleteOpen: true });

  render () {
    const { name, description } = this.props;

    return (
      <List.Item className='Projects-list__item'>
        <List.Icon name='file code' size='large' verticalAlign='middle' />
        <List.Content>
          <List.Header>{name}</List.Header>
          <List.Description>{description}</List.Description>
        </List.Content>
        <List.Content>
          <Button 
            onClick={this.teamProject}
            className='Projects-list__link'
            color='green' 
            size='small'>
            <Icon name='users'/>Team
          </Button>
        </List.Content>
        <List.Content>
          <Button 
            onClick={this.tasksProject}
            className='Projects-list__link'
            color='green' 
            size='small'>
            <Icon name='tasks'/>Tasks
          </Button>
        </List.Content>
      </List.Item>
    );
  }  
}

ProjectsItem.contextTypes = {
  router: PropTypes.object.isRequired
};

ProjectsItem.propTypes = {
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired
};

export default ProjectsItem;