const apiUrl = 'http://localhost:8000';

export const employeesUrl = () => `${apiUrl}/employees`;
export const employeeUrl = id => `${employeesUrl()}/${id}`;
export const projectsUrl = () => `${apiUrl}/projects`;
export const projectAddEmployeeUrl = id => `${apiUrl}/projects/${id}`;
export const projectEmployeesUrl = id => `${apiUrl}/projects/${id}/employees`;
export const projectTasksUrl = id => `${apiUrl}/projects/${id}/tasks`;
export const employeeTasksUrl = id => `${apiUrl}/employees/${id}/tasks`;
export const authUrl = () => `${apiUrl}/auth`;
export const locationsUrl = () => `${apiUrl}/locations`;
export const positionsUrl = () => `${apiUrl}/positions`;
export const statusesUrl = () => `${apiUrl}/statuses`;
export const typeTaskUrl = () => `${apiUrl}/typetask`;
export const taskUrl = id => `${apiUrl}/tasks/${id}`;
export const taskAddUrl = () => `${apiUrl}/tasks`;
