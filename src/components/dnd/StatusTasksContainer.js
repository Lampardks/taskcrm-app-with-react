import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { DropTarget } from 'react-dnd';

import Tasks from './Tasks';

const cardTarget = {
  canDrop () {
    return false;
  },
  hover () {
    return false;
  }
};

class StatusTasksContainer extends Component {
  render () {
    const { 
      connectDropTarget, 
      status, 
      tasks, 
      employees, 
      x, 
      moveCard, 
      typeTask,
      onRemove,
      updateTask
    } = this.props;

    return connectDropTarget(
      <div className='four wide column'>
        <div className='status-container'>
          <div className='status-container__title'>
            {status.name}
          </div>
          <Tasks 
            moveCard={moveCard}
            tasks={tasks}
            employees={employees}
            typeTask={typeTask}
            statusId={status.id}
            x={x}
            onRemove={onRemove}
            updateTask={updateTask} />
        </div>
      </div>
    );
  }
}

StatusTasksContainer.propTypes = {
  status: PropTypes.object.isRequired,
  tasks: PropTypes.array.isRequired,
  employees: PropTypes.array.isRequired,
  typeTask: PropTypes.array,
  connectDropTarget: PropTypes.func.isRequired,
  x: PropTypes.number,
  moveCard: PropTypes.func.isRequired,
  onRemove: PropTypes.func.isRequired,
  updateTask: PropTypes.func.isRequired
};

export default 
DropTarget('task', cardTarget, (connect, monitor) => ({
  connectDropTarget: connect.dropTarget(),
  isOver: monitor.isOver(),
  canDrop: monitor.canDrop()
}))(StatusTasksContainer);
