import axios from 'axios';
import { 
  projectTasksUrl, 
  statusesUrl, 
  taskUrl, 
  typeTaskUrl, 
  taskAddUrl,
  employeeTasksUrl 
} from '../../urls';

export const getProjectTasks = id => dispatch => {
  return axios.get(projectTasksUrl(id))
    .then(({ data }) => 
      dispatch({
        type: 'GET_TASKS',
        payload: data
      })
    );
};

export const getEmployeeTasks = id => dispatch => {
  return axios.get(employeeTasksUrl(id))
    .then(({ data }) => 
      dispatch({
        type: 'GET_TASKS',
        payload: data
      })
    );
};

export const getStatuses = () => dispatch => {
  return axios.get(statusesUrl())
    .then(({ data }) => 
      dispatch({
        type: 'GET_STATUSES',
        payload: data
      })
    );
};

export const moveCard = (projectId, id, lastX, lastY, nextX, nextY) => dispatch => {
  dispatch({
    type: 'MOVE_TASK',
    payload: {
      lastX, 
      lastY, 
      nextX, 
      nextY
    }
  });

  return axios.get(projectTasksUrl(projectId))
    .then(({ data }) => {
      const tasksStatus = data.filter((task) => {
        return task.projectId == projectId
          && task.statusId === nextX;
      });
      const newTasksStatus = data.filter((task) => {
        return task.projectId == projectId
          && task.statusId === nextX 
          && task.sortId >= nextY;
      });

      const lastTasksStatus = data.filter((task) => {
        return task.projectId == projectId
          && task.statusId === lastX
          && task.sortId > lastY;
      });

      newTasksStatus.forEach((task) => {
        axios.put(taskUrl(task.id), { sortId: task.sortId + 1 });
      });

      return Promise.all([
        Promise.resolve('val1'),
        tasksStatus,
        lastTasksStatus
      ]);
    })
    .then((tasksStatus) => {
      if (tasksStatus[1].length === 0) {
        nextY = 0;
      } else if (tasksStatus[1].length < nextY) {
        nextY = tasksStatus[1].length;
      }

      return axios.put(taskUrl(id), { statusId: nextX, sortId: nextY })
        .then(() => {
          if (tasksStatus[2].length !== 0) {
            tasksStatus[2].forEach((task, i) => {
              axios.put(taskUrl(task.id), { sortId: task.sortId - 1 })
                .then(() => {
                  if (tasksStatus[2].length === i + 1) {
                    dispatch(getProjectTasks(projectId));
                  }
                });
            });
          }
          else {
            dispatch(getProjectTasks(projectId));
          }
        });
    });
};

export const getTypeTask = () => dispatch => {
  return axios.get(typeTaskUrl())
    .then(({ data }) => 
      dispatch({
        type: 'GET_TYPE_TASK',
        payload: data
      })
    );
};

export const addTask = task => dispatch => {
  return axios.get(projectTasksUrl(task.projectId))
    .then(({ data }) => {
      const tasksStatus = data.filter((task) => {
        return task.projectId == task.projectId
          && task.statusId === 0;
      });

      tasksStatus.forEach((task) => {
        axios.put(taskUrl(task.id), { sortId: task.sortId + 1 });
      });
    })
    .then(() => {
      return axios.post(taskAddUrl(), task)
        .then(() => 
          dispatch(getProjectTasks(task.projectId))
        );
    });
};

export const onRemove = (taskId, projectId) => dispatch => {
  return axios.delete(taskUrl(taskId))
    .then(() => 
      dispatch(getProjectTasks(projectId))
    );
};

export const updateTask = task => dispatch => {
  return axios.put(taskUrl(task.id), { ...task })
    .then(({ data }) => {
      dispatch({
        type: 'UPDATED_TASK',
        payload: data
      });
      // dispatch(getProjectTasks(data.projectId));
    }
    );
};