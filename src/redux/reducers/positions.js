const initialState =  {
  positions: []
};

const positions = (state = initialState, action) => {
  switch (action.type) {
    case 'GET_POSITIONS':
      return { ...state, positions: action.payload };
    default:
      return state;
  }
};

export default positions;