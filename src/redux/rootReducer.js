import { combineReducers } from 'redux';
import user from './reducers/auth';
import employees from './reducers/employees';
import projects from './reducers/projects';
import tasks from './reducers/tasks';
import locations from './reducers/locations';
import positions from './reducers/positions';
import statuses from './reducers/statuses';
import typeTask from './reducers/typeTask';

export default combineReducers({
  user,
  employees,
  projects,
  tasks,
  locations,
  positions,
  statuses,
  typeTask
});
